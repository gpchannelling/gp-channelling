package com.gpchanneling.gpc.enums;

/**
 * Created by ganusha on 12/10/2018.
 */
public enum Status {
    INIT,
    PENDING,
    ACTIVE,
    INACTIVE,
    COMPLETE;
}
