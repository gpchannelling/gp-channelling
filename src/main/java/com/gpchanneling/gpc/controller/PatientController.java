package com.gpchanneling.gpc.controller;

import com.gpchanneling.gpc.dto.*;
import com.gpchanneling.gpc.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 9/27/2018.
 */
@RestController
@RequestMapping(value = "/patients" )
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping(value = "" )
    public ResponseEntity<List<Patient>> getPatients(@RequestParam(defaultValue = "null") Map<String,Double> params) {

        List<Patient> patientList = new ArrayList<>();
        patientList.add(new Patient());
        patientList.add(new Patient());

        //return new ResponseEntity<>(patientList, HttpStatus.OK);
        return new ResponseEntity<>(patientService.getPatients(), HttpStatus.OK);
    }

    @GetMapping(value = "/{patientId}" )
    public ResponseEntity<Patient> getPatientById(@PathVariable int patientId) {

      //  return new ResponseEntity<>(new Patient(), HttpStatus.OK);
      return new ResponseEntity<>(patientService.getPatientById(patientId), HttpStatus.OK);
    }

    @PostMapping(value = "")
    public ResponseEntity<Patient> savePatient(@RequestBody Patient patient) {
    patient.setDiagnosesDetails(null);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    @PutMapping(value = "/{patientId}")
    public ResponseEntity<Patient> updatePatient(@RequestBody Patient patient) {
        patient.setDiagnosesDetails(null);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    @GetMapping(value = "/{patientId}/diagnoses")
    public ResponseEntity<DiagnosesDetail> getDiagnosesDetail(){
        Appointment appointment=new Appointment();
        DiagnosesDetail diagnosesDetail=new DiagnosesDetail();
        diagnosesDetail.setAppointment(appointment);
        return new ResponseEntity<>(diagnosesDetail,HttpStatus.OK);
    }

    @PostMapping(value = "/{patientId}/diagnoses")
    public ResponseEntity<DiagnosesDetail> saveDiagnosesDetail(){
        Appointment appointment=new Appointment();
        DiagnosesDetail diagnosesDetail=new DiagnosesDetail();
        diagnosesDetail.setAppointment(appointment);
        return new ResponseEntity<>(diagnosesDetail,HttpStatus.OK);
    }

    @PutMapping(value = "/{patientId}/diagnoses/{diagnosesId}")
    public ResponseEntity<DiagnosesDetail> updateDiagnosesDetail(){
        Appointment appointment=new Appointment();
        DiagnosesDetail diagnosesDetail=new DiagnosesDetail();
        diagnosesDetail.setAppointment(appointment);
        return new ResponseEntity<>(diagnosesDetail,HttpStatus.OK);
    }

    @GetMapping(value = "/{patientId}/appointments")
    public ResponseEntity<List<Appointment>> getAppointmentsByPatientId(){

        List<com.gpchanneling.gpc.dto.Appointment> appointmentList =new ArrayList<>();
        com.gpchanneling.gpc.dto.Appointment appointment=new com.gpchanneling.gpc.dto.Appointment();
        List<Payment> paymentList=new ArrayList<>();
        paymentList.add(new Payment());
        List<AppointmentCharge> chargesList=new ArrayList<>();
        chargesList.add(new AppointmentCharge());
        Session session=new Session();
        session.setDoctor(new Doctor());
        appointment.setSession(session);
        appointment.setCenter(new Center());
        appointment.setPayments(paymentList);
        appointment.setAppointmentCharge(chargesList);
        appointmentList.add(appointment);
        return new ResponseEntity<>(appointmentList,HttpStatus.OK);
    }


}
