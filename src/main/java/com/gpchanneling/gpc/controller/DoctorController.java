package com.gpchanneling.gpc.controller;

import com.gpchanneling.gpc.data.entity.Qualification;
import com.gpchanneling.gpc.dto.Doctor;
import com.gpchanneling.gpc.dto.Session;
import com.gpchanneling.gpc.dto.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ganusha on 12/3/2018.
 */
@RestController
@RequestMapping(value = "/doctors")
public class DoctorController {
    @GetMapping(value = "/{doctorId}")
    public ResponseEntity<Doctor> getCenters() {
        Doctor doctor=new Doctor();
        List<Session> sessionList=new ArrayList<>();
        sessionList.add(new Session());
        User user=new User();

        doctor.setUser(user);
        doctor.setQualification(new Qualification());

        return new ResponseEntity<>(doctor, HttpStatus.OK);
    }


    @PutMapping(value = "/{doctorId}")
    public  ResponseEntity<Doctor> updateDoctor(){
        Doctor doctor=new Doctor();
        List<Session> sessionList=new ArrayList<>();
        sessionList.add(new Session());
        User user=new User();
        doctor.setUser(user);
        doctor.setQualification(new Qualification());
        return new ResponseEntity<>(doctor, HttpStatus.OK);
    }

    @GetMapping(value = "")
    public ResponseEntity<List<Doctor>> getDoctorsByName(){
        List<Doctor> doctorList=new ArrayList<>();
        Doctor doctor=new Doctor();
        List<Session> sessionList=new ArrayList<>();
        sessionList.add(new Session());
        User user=new User();
        doctor.setUser(user);
        doctor.setQualification(new Qualification());
        doctorList.add(doctor);
       return new ResponseEntity<>(doctorList,HttpStatus.OK);
    }
}
