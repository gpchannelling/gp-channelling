package com.gpchanneling.gpc.controller;

import com.gpchanneling.gpc.data.entity.Privilege;
import com.gpchanneling.gpc.data.entity.Role;
import com.gpchanneling.gpc.dto.User;
import com.gpchanneling.gpc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ganusha on 10/26/2018.
 */
@RestController
@RequestMapping(value = "/users" )
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/signup" )
    @ResponseBody
    public ResponseEntity<User> signUpUser(@RequestBody User user) {
        User userModel=new User();
        List<Role> roleList=new ArrayList<>();
        List<Privilege> privilegeList=new ArrayList<>();
        privilegeList.add(new Privilege());
        Role role=new Role();
        role.setPrivileges(privilegeList);
        roleList.add(role);
        userModel.setRoles(roleList);
        return new ResponseEntity<>(userModel, HttpStatus.OK);
        // return userService.saveUser(user);

    }

    @PostMapping(value = "/signin")
    public ResponseEntity<User> signInUser(@RequestBody User user){
        User userModel=new User();
        List<Role> roleList=new ArrayList<>();
        List<Privilege> privilegeList=new ArrayList<>();
        privilegeList.add(new Privilege());
        Role role=new Role();
        role.setPrivileges(privilegeList);
        roleList.add(role);
        userModel.setRoles(roleList);
        return new ResponseEntity<>(userModel, HttpStatus.OK);
        //return userService.getUser(user.getUserName(),user.getPassword(),user.getToken());

    }

    @PostMapping(value = "/reset")
    public ResponseEntity<User> resetUser(@RequestBody User user){

        User userModel=new User();
        List<Role> roleList=new ArrayList<>();
        List<Privilege> privilegeList=new ArrayList<>();
        privilegeList.add(new Privilege());
        Role role=new Role();
        role.setPrivileges(privilegeList);
        roleList.add(role);
        userModel.setRoles(roleList);
        return new ResponseEntity<>(userModel, HttpStatus.OK);
        //return userService.getUser(user.getUserName(),user.getPassword(),user.getToken());

    }

    @PostMapping(value = "/recover")
    public ResponseEntity<User> recoverUser(@RequestBody User user){

        User userModel=new User();
        List<Role> roleList=new ArrayList<>();
        List<Privilege> privilegeList=new ArrayList<>();
        privilegeList.add(new Privilege());
        Role role=new Role();
       role.setPrivileges(privilegeList);
        roleList.add(role);
        userModel.setRoles(roleList);
        return new ResponseEntity<>(userModel, HttpStatus.OK);
        //return userService.getUser(user.getUserName(),user.getPassword(),user.getToken());

    }



//
//    @RequestMapping(value = "roles",method = RequestMethod.GET)
//    public List<Role> getRoles(){
//        return userService.getRoles();
//
//    }
//
//    @RequestMapping(value = "roles", method =RequestMethod.POST )
//    public void saveRole(@RequestBody Role role){
//        userService.saveRole(role);
//    }
//
////    @RequestMapping(value = "test", method =RequestMethod.POST )
////    public JsonResponse test() {
////        JsonResponse response = new JsonResponse();
////        response.setCode(HttpStatus.NOT_ACCEPTABLE);
////        response.setMessage("Email  already registered");
////        return response;
////    }


}
