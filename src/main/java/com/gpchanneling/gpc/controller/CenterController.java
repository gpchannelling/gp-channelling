package com.gpchanneling.gpc.controller;


import com.gpchanneling.gpc.data.entity.Address;
import com.gpchanneling.gpc.dto.Center;
import com.gpchanneling.gpc.services.CenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 10/22/2018.
 */
@RestController
@RequestMapping(value = "/centers")
public class CenterController {

    @Autowired
    private CenterService centerService;

    //get all registered medical centers
    @GetMapping(value = "")
    public ResponseEntity<List<Center>> getCenters(@RequestParam(defaultValue = "null") Map<String,Double> params ) {
        List<Center> centerList=new ArrayList<>();
        List<Center> doctorList=new ArrayList<>();
        Address address= new Address();
        Center center=new Center();
        center.setAddress(address);
        centerList.add(center);
       // return new ResponseEntity<>(centerList, HttpStatus.OK);
        return new ResponseEntity<>(centerService.getCenters(params),HttpStatus.OK);
    }

}
