package com.gpchanneling.gpc.controller;


import com.gpchanneling.gpc.data.entity.Qualification;
import com.gpchanneling.gpc.dto.*;
import com.gpchanneling.gpc.services.AppointmentService;
import com.gpchanneling.gpc.services.CenterService;
import com.gpchanneling.gpc.services.DoctorService;
import com.gpchanneling.gpc.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 11/27/2018.
 */
@RestController
@RequestMapping(value = "/centers")
public class CenterDoctorController {

    @Autowired
    private CenterService centerService;
    @Autowired
    private AppointmentService appointmentService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private SessionService sessionService;

    @GetMapping(value = "/{centerId}/doctors")

    public ResponseEntity<List<Doctor>> getDoctorsByCenterId(@PathVariable Map<String, Integer> params) {

        List<Doctor> doctorList = new ArrayList<>();
        List<Session> sessionList = new ArrayList<>();
        Doctor doctor = new Doctor();
        User user = new User();
        user.setRoles(null);
        doctor.setUser(user);
        sessionList.add(new Session());
        doctor.setQualification(new Qualification());
        doctorList.add(doctor);
        //return new ResponseEntity<>(doctorList, HttpStatus.OK);
        return new ResponseEntity<>(doctorService.getDoctorsByCenterId(params), HttpStatus.OK);


    }

    @GetMapping(value = "/{centerId}/doctors/{doctorId}/sessions")
    public ResponseEntity<List<Session>> getSessionsByDoctorIdAndCenterId(@PathVariable Map<String, Integer> params) {

        List<Session> sessionList = new ArrayList<>();
        sessionList.add(new Session());
        sessionList.add(new Session());
        //return new ResponseEntity<>(sessionList, HttpStatus.OK);

        return new ResponseEntity<>(sessionService.getSessionsByDoctorIdAndCenterId(params), HttpStatus.OK);


    }

    @GetMapping(value = "/{centerId}/doctors/{doctorId}/sessions/{sessionId}")
    public ResponseEntity<Session> getSessionByDoctorIdAndCenterId(@PathVariable Map<String, Integer> params) {

        //return new ResponseEntity<>(new Session(), HttpStatus.OK);
        return new ResponseEntity<>(sessionService.getSessionBySessionId(params), HttpStatus.OK);

    }

    @PostMapping(value = "/{centerId}/doctors/{doctorId}/sessions")
    public ResponseEntity<Session> saveSession(@PathVariable Map<String, Integer> params, @RequestBody Session session) {
        //session.getDoctor().getUser().setRoles(null);
        //return new ResponseEntity<>(session, HttpStatus.OK);
        return new ResponseEntity<Session>(sessionService.saveSession(params, session), HttpStatus.OK);

    }

    @PutMapping(value = "/{centerId}/doctors/{doctorId}/sessions/{sessionId}")
    public ResponseEntity<Session> updateSession(@PathVariable Map<String, Integer> params, @RequestBody Session session) {
//        session.getDoctor().getUser().setRoles(null);
//        return new ResponseEntity<>(session, HttpStatus.OK);
        return new ResponseEntity<Session>(sessionService.updateSession(params, session), HttpStatus.OK);

    }

    @GetMapping(value = "{centerId}/doctors/{doctorId}/sessions/{sessionId}/appointments")
    public ResponseEntity<List<Appointment>> getAppointment(@PathVariable Map<String, Integer> params) {
        List<Appointment> appointmentList = new ArrayList<>();
        Appointment appointment = new Appointment();
        List<Payment> paymentList = new ArrayList<>();
        paymentList.add(new Payment());
        List<AppointmentCharge> chargesList = new ArrayList<>();
        chargesList.add(new AppointmentCharge());
        Session session = new Session();
        session.setDoctor(new Doctor());
        appointment.setSession(session);
        appointment.setCenter(new Center());
        appointment.setPatient(new Patient());
        appointment.setPayments(paymentList);
        appointment.setAppointmentCharge(chargesList);
        appointmentList.add(appointment);

        //return new ResponseEntity<>(appointmentList, HttpStatus.OK);
        return new ResponseEntity<>(appointmentService.getAppointments(params), HttpStatus.OK);
    }

    @GetMapping(value = "{centerId}/doctors/{doctorId}/sessions/{sessionId}/appointments/{refNo}")
    public ResponseEntity<Appointment> getAppointmentByRefNo(@PathVariable Map<String, Integer> params) {
        Appointment appointment = new Appointment();
        List<Payment> paymentList = new ArrayList<>();
        paymentList.add(new Payment());
        List<AppointmentCharge> chargesList = new ArrayList<>();
        chargesList.add(new AppointmentCharge());
        Session session = new Session();
        session.setDoctor(new Doctor());
        appointment.setSession(session);
        appointment.setCenter(new Center());
        appointment.setPatient(new Patient());
        appointment.setPayments(paymentList);
        appointment.setAppointmentCharge(chargesList);
        //return new ResponseEntity<>(appointment, HttpStatus.OK);

        return new ResponseEntity<>(appointmentService.getAppointmentByRefNo(params), HttpStatus.OK);
    }

    @PostMapping(value = "{centerId}/doctors/{doctorId}/sessions/{sessionId}/appointments")
    public ResponseEntity<Appointment> saveAppointment(@PathVariable Map<String, Integer> params,@RequestBody Appointment appointment) {
        Appointment newAppointment = new Appointment();
        List<Payment> paymentList = new ArrayList<>();
        paymentList.add(new Payment());
        List<AppointmentCharge> chargesList = new ArrayList<>();
        chargesList.add(new AppointmentCharge());
        Session session = new Session();
        session.setDoctor(new Doctor());
        newAppointment.setSession(session);
        newAppointment.setCenter(new Center());
        newAppointment.setPatient(new Patient());
        newAppointment.setPayments(paymentList);
        newAppointment.setAppointmentCharge(chargesList);
        // return new ResponseEntity<>(newAppointment, HttpStatus.OK);
        return new ResponseEntity<>(appointmentService.saveAppointment(params,appointment), HttpStatus.OK);
    }

    @PutMapping(value = "{centerId}/doctors/{doctorId}/sessions/{sessionId}/appointments/{refNo}")
    public ResponseEntity<Appointment> updateAppointment(@PathVariable Map<String, Integer> params,@RequestBody Appointment appointment) {
        Appointment newAppointment = new Appointment();
        List<Payment> paymentList = new ArrayList<>();
        paymentList.add(new Payment());
        List<AppointmentCharge> chargeList = new ArrayList<>();
        chargeList.add(new AppointmentCharge());
        Session session = new Session();
        session.setDoctor(new Doctor());
        newAppointment.setSession(session);
        newAppointment.setCenter(new Center());
        newAppointment.setPatient(new Patient());
        newAppointment.setPayments(paymentList);
        newAppointment.setAppointmentCharge(chargeList);
        //return new ResponseEntity<>(newAppointment, HttpStatus.OK);
        return new ResponseEntity<>(appointmentService.updateAppointment(params, appointment), HttpStatus.OK);
    }


}
