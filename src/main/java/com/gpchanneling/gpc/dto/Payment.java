package com.gpchanneling.gpc.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Created by ganusha on 11/28/2018.
 */
@Getter@Setter
public class Payment {

    private int paymentId;
    private LocalDate paymentDate;
    private String paymentMethod;
    private double amount;

}
