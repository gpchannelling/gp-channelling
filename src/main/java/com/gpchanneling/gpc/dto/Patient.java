package com.gpchanneling.gpc.dto;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by ganusha on 11/21/2018.
 */
@Getter@Setter
public class Patient {

    private int patientId;
    private String mobileNo;
    private String nic;
    private LocalDate dateOfBirth;
    private String name;
    private String type;
    private List<DiagnosesDetail> diagnosesDetails;


}
