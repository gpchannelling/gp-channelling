package com.gpchanneling.gpc.dto;

import com.gpchanneling.gpc.data.entity.Qualification;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by ganusha on 11/19/2018.
 */
@Getter@Setter
public class Doctor {

    private int doctorId;
    private User user;
    private String slmcNo;
    private Qualification qualification;

}
