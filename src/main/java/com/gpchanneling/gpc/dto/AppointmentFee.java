package com.gpchanneling.gpc.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by ganusha on 12/13/2018.
 */
@Getter@Setter
public class AppointmentFee {

    private int feeId;
//    private Session session;
    private String feeCode;
    private double amount;
}
