package com.gpchanneling.gpc.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by ganusha on 12/4/2018.
 */
@Getter@Setter
public class DiagnosesDetail {

    private int detailId;
    private Appointment appointment;
    private double weight;
    private double blood_pressure;
    private  String description;



}
