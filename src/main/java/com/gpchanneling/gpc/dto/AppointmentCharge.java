package com.gpchanneling.gpc.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by ganusha on 11/28/2018.
 */
@Getter@Setter
public class AppointmentCharge {

    private int chargeId;
//   private Appointment appointment;
    private String chargeCode;
    private double amount;

}
