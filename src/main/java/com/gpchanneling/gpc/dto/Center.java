package com.gpchanneling.gpc.dto;


import com.gpchanneling.gpc.data.entity.Address;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by ganusha on 11/21/2018.
 */
@Getter@Setter
public class Center {
    private int centerId;
    private String centerName;
    private Address address;
    private List<Doctor> doctors;

}
