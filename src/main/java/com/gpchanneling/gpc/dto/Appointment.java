package com.gpchanneling.gpc.dto;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;
import java.util.List;

/**
 * Created by ganusha on 11/19/2018.
 */
@Getter @Setter
public class Appointment {

    private int refNo;
    private Patient patient;
    private int appointmentNo;
    private LocalTime appointmentTime;
    private String status;
    private List<AppointmentCharge> appointmentCharge;
    private List<Payment> payments;
    private Center center;
    private Session session;

}
