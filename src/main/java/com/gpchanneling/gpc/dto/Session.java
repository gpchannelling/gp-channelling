package com.gpchanneling.gpc.dto;

import com.gpchanneling.gpc.enums.Status;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;


/**
 * Created by ganusha on 11/19/2018.
 */
@Getter@Setter
public class Session {

    private int sessionId;
    private int noOfPatient;
    private LocalTime startTime;
    private LocalTime endTime;
    private LocalDate appointmentDate;
    private Status status;
    private int currentPatient;
    private int noOfReservedPatient;
    private Doctor doctor;
    private List<AppointmentFee> appointmentFees;



}
