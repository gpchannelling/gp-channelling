package com.gpchanneling.gpc.dto;

import com.gpchanneling.gpc.data.entity.Role;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by ganusha on 11/29/2018.
 */
@Getter@Setter
public class User {
    private int userId;
    private String firstName;
    private String lastName;
    private String email;
    private String status;
    private Date dateCreated;
    private String userType;
    private String nic;
    private String telephoneNo;
    private List<Role> roles;
}
