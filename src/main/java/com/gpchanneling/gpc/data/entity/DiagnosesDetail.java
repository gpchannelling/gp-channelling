package com.gpchanneling.gpc.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by ganusha on 11/19/2018.
 */
@Entity
@Table(name = "diagnoses_detail")
@Getter@Setter
public class DiagnosesDetail {

    @Id
    @Column(name="detail_id")
    private int detailId;

    @ManyToOne
    @JoinColumn(name="ref_no")
    private Appointment appointment;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name="weight")
    private double weight;

    @Column(name = "blood_pressure")
    private double blood_pressure;

    @Column(name = "description")
    private  String description;


}
