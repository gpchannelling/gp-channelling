package com.gpchanneling.gpc.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Created by ganusha on 10/2/2018.
 */
@Entity
@Table(name="payment")
@Getter
@Setter
public class Payment {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="payment_id")
    private int paymentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ref_no")
    private Appointment appointment;

    @Column(name="payment_date")
    private LocalDate paymentDate;

    @Column(name = "payment_method")
    private String PaymentMethod;

    @Column(name = "amount")
    private double amount;

}
