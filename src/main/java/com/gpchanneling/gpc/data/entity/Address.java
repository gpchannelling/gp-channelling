package com.gpchanneling.gpc.data.entity;

import javax.persistence.*;

/**
 * Created by ganusha on 9/27/2018.
 */
@Entity(name="address")
@Table(name="address")
public class Address  {
    @Id
    @Column(name="address_id")
    private int addressId;

    @Column(name="city")
    private String city;

    @Column(name="latitude")
    private double latitude;

    @Column(name="longitude")
    private double longitude;

    @Column(name="address")
    private String address;


    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
