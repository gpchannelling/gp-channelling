package com.gpchanneling.gpc.data.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by ganusha on 10/2/2018.
 */
@Entity
@Table(name="appointment_charge ")
@Setter@Getter
public class AppointmentCharge {

    @Id
    @Column(name="charge_id")
    private int chargeId;

    @ManyToOne
    @JoinColumn(name="ref_no")
    private Appointment appointment;

    @Column(name="charge_code")
    private String chargeCode;

    @Column(name="amount")
    private double amount;


}
