package com.gpchanneling.gpc.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.List;

/**
 * Created by ganusha on 9/27/2018.
 */
@Entity
@Table(name="appointment")
@Getter
@Setter
public class Appointment {
    @Id
    @Column(name="ref_no")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int refNo;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @ManyToOne
    @JoinColumn(name = "session_id")
    private Session session;

    @Column(name="appointment_no")
    private int appointmentNo;

    @Column(name = "appointment_time")
    private LocalTime appointmentTime;

    @OneToMany(mappedBy="appointment")
    private List<Payment> payments;

    @Column(name="status")
    private String status;

}
