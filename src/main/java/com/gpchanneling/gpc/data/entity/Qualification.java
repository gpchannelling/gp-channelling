package com.gpchanneling.gpc.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by ganusha on 9/27/2018.
 */

@Entity
@Table(name="qualification")
public class Qualification {
    @Id
    @Column(name="qualification_id")
    private int qualificationId;
    @Column(name="university")
    private String university;
    @Column(name="education")
    private String eduaction;
    @Column(name="remark")
    private String remark;

    public String getUniversity() {
        return university;
    }

    public int getQualificationId() {
        return qualificationId;
    }

    public String getEduaction() {
        return eduaction;
    }

    public String getRemark() {
        return remark;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public void setQualificationId(int qualificationId) {
        this.qualificationId = qualificationId;
    }

    public void setEduaction(String eduaction) {
        this.eduaction = eduaction;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
