package com.gpchanneling.gpc.data.entity;

import javax.persistence.*;

/**
 * Created by ganusha on 10/5/2018.
 */
@Entity
@Table(name="patient_account")
public class PatientAccount {

    @Id
    @Column(name="account_id")
    private int accountid;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public int getAccountid() {
        return accountid;
    }

    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
