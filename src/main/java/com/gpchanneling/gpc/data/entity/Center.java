package com.gpchanneling.gpc.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ganusha on 9/27/2018.
 */
@Entity
@Table(name="institute")
@Getter@Setter
public class Center {
    @Id
    @Column(name="institute_id")
    private int centerId;

    @Column(name="institute_name")
    private  String centerName;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "doctor_institute",
            joinColumns = @JoinColumn(name = "institute_id"),
            inverseJoinColumns = @JoinColumn(name = "doctor_id"))
    private List<Doctor> doctors;


}
