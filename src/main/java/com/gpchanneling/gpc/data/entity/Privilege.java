package com.gpchanneling.gpc.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by ganusha on 10/2/2018.
 */
@Entity
@Table(name="privilege")
@Getter
@Setter
public class Privilege {

    @Id
    @Column(name = "privilege_id")
    private int privilegeId;

    @Column(name = "description")
    private String description;

    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "privilege_code")
    private String privilegeCode;

}