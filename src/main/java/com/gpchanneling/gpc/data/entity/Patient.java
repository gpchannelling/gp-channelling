package com.gpchanneling.gpc.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Created by ganusha on 9/27/2018.
 */
@Entity
@Table(name = "patient")
@Getter
@Setter
public class Patient {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "patient_id")
    private int patientId;

    @Column(name = "mobile_no")
    private String mobileNo;

    @Column(name = "nic")
    private String nic;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;


}
