package com.gpchanneling.gpc.data.entity;

import com.gpchanneling.gpc.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;


/**
 * Created by ganusha on 9/26/2018.
 */
@Entity
@Table(name = "session")
@Getter
@Setter
public class Session {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "session_id")
    private int sessionId;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @ManyToOne
    @JoinColumn(name = "institute_id")
    private Center center;

    @Column(name = "no_of_patient")
    private int noOfPatient;

   // @Temporal(TemporalType.TIME)
    @Column(name = "start_time")
    private LocalTime startTime;

    //@Temporal(TemporalType.TIME)
    @Column(name = "end_time")
    private LocalTime endTime;


   // @Temporal(TemporalType.DATE)
    @Column(name = "appointment_date")
    private LocalDate appointmentDate;

    @Enumerated(EnumType.STRING)
    //@Size(min=2, message="Name should have atleast 2 characters")
    @Column(name = "status")
    private Status status;

    @Column(name = "current_patient")
    private int currentPatient;

    @Column(name = "no_of_reserved_patient")
    private int noOfReservedPatient;

    @Column(name = "time_interval")
    private int timeInterval;

//    @OneToMany(mappedBy="session")
//    private List<AppointmentFee> appointmentFees;

}