package com.gpchanneling.gpc.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ganusha on 9/27/2018.
 */
@Entity
@Table(name="doctor")
@Setter@Getter
public class Doctor {

    @Id
    /*@GeneratedValue(strategy=GenerationType.AUTO)*/
    @Column(name="doctor_id")
    private int doctorId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "qualification_id")
    private Qualification qualification;

    @ManyToMany( mappedBy = "doctors")
    private List<Center> centers;

    @Column(name="slmc_no")
    private String slmcNo;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "session_id")
    private List<Session> session;



}
