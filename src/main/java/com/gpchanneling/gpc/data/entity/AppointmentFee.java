package com.gpchanneling.gpc.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by ganusha on 12/13/2018.
 */
@Entity(name = "appointment_fee")
@Table(name = "appointment_fee")
@Setter@Getter
public class AppointmentFee {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "fee_id")
    private int feeId;

    @ManyToOne
    @JoinColumn(name = "session_id")
    private Session session;

    @Column(name = "fee_code")
    private String feeCode;

    @Column(name = "amount")
    private double amount;
}
