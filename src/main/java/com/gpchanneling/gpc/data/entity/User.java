package com.gpchanneling.gpc.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by ganusha on 9/27/2018.
 */
@Entity
@Table(name = "user")
@Getter@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "username")
    private String userName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "status")
    private String status;

    @Column(name = "date_created")
    private LocalDate dateCreated;

    @Column(name = "user_type")
    private String userType;

    @Column(name = "token")
    private String token;

    @Column(name = "nic")
    private String nic;

    @Column(name = "telephone_no")
    private String telephoneNo;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "role_id"))
    private List<Role> roles;

    @ManyToMany
    @JoinTable(name = "user_institute",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "institute_id"))
    private List<Center> centers;



}
