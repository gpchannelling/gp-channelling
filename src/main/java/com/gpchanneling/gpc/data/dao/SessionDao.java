package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.Center;
import com.gpchanneling.gpc.data.entity.Doctor;
import com.gpchanneling.gpc.data.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ganusha on 9/26/2018.
 */
@Repository
public interface SessionDao extends JpaRepository<Session,Integer> {

    List<Session> findByDoctorAndCenter(Doctor doctor ,Center center);
}






