package com.gpchanneling.gpc.data.dao;


import com.gpchanneling.gpc.dto.Center;

import java.util.List;

/**
 * Created by ganusha on 12/6/2018.
 */
public interface LocationDao {
    List<Center> findLocationByCoordinates(double latitude,double longitute);
}
