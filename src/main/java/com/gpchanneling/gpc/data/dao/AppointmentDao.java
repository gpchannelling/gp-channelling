package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.Appointment;
import com.gpchanneling.gpc.data.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by ganusha on 10/15/2018.
 */

public interface AppointmentDao extends JpaRepository<Appointment,Integer> {
    List<Appointment> findBySession(Session session);


}
