package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.Center;
import com.gpchanneling.gpc.data.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by ganusha on 10/4/2018.
 */

public interface DoctorDao extends JpaRepository<Doctor, Integer> {

    List<Doctor> findByCenters(Center center);
}
