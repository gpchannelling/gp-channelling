package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ganusha on 10/12/2018.
 */
public interface PatientDao extends JpaRepository<Patient,Integer>{
}
