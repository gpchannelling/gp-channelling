package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.Center;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ganusha on 9/27/2018.
 */


public interface CenterDao extends JpaRepository<Center,Integer> {



//    @Query(value = "select * from (SELECT i.institute_id,i.institute_name,i.address_id,a.city,a.longitude,a.latitude,a.address, " +
//            "111.045 * DEGREES(ACOS(COS(RADIANS(?1))\n" +
//            "  * COS(RADIANS(latitude))\n" +
//            "  * COS(RADIANS(longitude) - RADIANS(?2))\n" +
//            "   + SIN(RADIANS(?1))\n" +
//            "    * SIN(RADIANS(latitude))))\n" +
//            "     AS distance_in_km\n" +
//            "      FROM institute i inner join address a on i.address_id=a.address_id\n" +
//            "      )as aaa where distance_in_km <10", nativeQuery = true)
//    List<Center> getCenterByAddressLatitudeAndAddressLongitude();
}
