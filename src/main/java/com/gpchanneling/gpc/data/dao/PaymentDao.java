package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ganusha on 12/13/2018.
 */
public interface PaymentDao extends JpaRepository<Payment,Integer> {
}
