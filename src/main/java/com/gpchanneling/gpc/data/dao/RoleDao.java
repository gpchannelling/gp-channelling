package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ganusha on 11/5/2018.
 */
public interface RoleDao extends JpaRepository<Role,Integer> {

    Role findByRoleName(String roleName);
}
