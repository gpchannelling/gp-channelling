package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.AppointmentFee;
import com.gpchanneling.gpc.data.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by ganusha on 12/18/2018.
 */
public interface AppointmentFeeDao extends JpaRepository<AppointmentFee,Integer> {
    List<AppointmentFee> findBySession(Session session);
}
