package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.Appointment;
import com.gpchanneling.gpc.data.entity.AppointmentCharge;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by ganusha on 12/13/2018.
 */
public interface AppointmentChargeDao extends JpaRepository<AppointmentCharge,Integer> {
    List<AppointmentCharge> findByAppointment(Appointment appointment);
}
