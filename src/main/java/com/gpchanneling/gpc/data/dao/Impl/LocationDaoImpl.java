package com.gpchanneling.gpc.data.dao.Impl;

import com.gpchanneling.gpc.data.dao.LocationDao;
import com.gpchanneling.gpc.data.entity.Address;
import com.gpchanneling.gpc.dto.Center;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ganusha on 12/6/2018.
 */
@Repository
public class LocationDaoImpl implements LocationDao {

    @Autowired
    EntityManagerFactory emf;
    @Override
    public List<Center> findLocationByCoordinates(double lat, double lon) {


//        System.out.println("************lon*********" + lon);
//        System.out.println("************lat***********" + lat);

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
//Testing ------------------------------------------------------------------------
        Query query = em.createNativeQuery("select * from (SELECT i.institute_id,i.institute_name,i.address_id,a.city,a.longitude,a.latitude,a.address, 111.045 * DEGREES(ACOS(COS(RADIANS(?))\n" +
                "  * COS(RADIANS(latitude))\n" +
                "  * COS(RADIANS(longitude) - RADIANS(?))\n" +
                "  + SIN(RADIANS(?))\n" +
                "  * SIN(RADIANS(latitude))))\n" +
                "  AS distance_in_km\n" +
                " FROM institute i inner join address a on i.address_id=a.address_id\n" +
                ")as aaa where distance_in_km <10\n" +
                " \n");


        query.setParameter(1, lat);
        query.setParameter(2, lon);
        query.setParameter(3, lat);

        List<Object[]> centerList = query.getResultList();
        List<com.gpchanneling.gpc.dto.Center> centers = new ArrayList<>();
        centerList.stream().forEach((record) -> {

            Address address = new Address();
            address.setAddressId((int) record[2]);
            address.setCity(record[3].toString());
            BigDecimal bd1=new BigDecimal(record[4].toString());
            address.setLongitude( bd1.doubleValue());
            BigDecimal bd2=new BigDecimal(record[5].toString());
            address.setLatitude(bd2.doubleValue());
            address.setAddress(record[6].toString());

            com.gpchanneling.gpc.dto.Center center = new com.gpchanneling.gpc.dto.Center();

            center.setCenterId((int) record[0]);
            center.setCenterName(record[1].toString());
            center.setAddress(address);
            centers.add(center);

        });


        em.getTransaction().commit();
        em.close();
        return centers;
    }
}
