package com.gpchanneling.gpc.data.dao;

import com.gpchanneling.gpc.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by ganusha on 10/26/2018.
 */
@Repository
public interface UserDao extends JpaRepository<User, Integer> {


    User findByToken(String token);

    @Query("select u from User u where u.userName = ?1 and password=?2")
    User findByUserNamePassword(String userName, String password);

    User findByUserName(String userName);

    User findByNic(String nic);

}
