package com.gpchanneling.gpc.services;


import com.gpchanneling.gpc.dto.Patient;

import java.util.List;

/**
 * Created by ganusha on 10/12/2018.
 */
public interface PatientService {
    List<Patient> getPatients();
    Patient getPatientById(int patientId);
    Patient savePatient(Patient map);
//    Patient savePatient(Patient patient);
//    Patient updatePatient(Patient patient, int PatientId);
}
