package com.gpchanneling.gpc.services;


import com.gpchanneling.gpc.dto.Session;

import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 9/26/2018.
 */
public interface SessionService {

    List<Session> getSessionsByDoctorIdAndCenterId(Map params);
    Session getSessionBySessionId(Map params);
    Session saveSession(Map params,Session session);
    Session updateSession(Map params, Session session);
}
