package com.gpchanneling.gpc.services;

import com.gpchanneling.gpc.dto.Center;

import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 9/27/2018.
 */
public interface CenterService {

    List<Center> getCenters(Map params);

}
