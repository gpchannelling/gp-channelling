package com.gpchanneling.gpc.services.impl;

import com.gpchanneling.gpc.data.dao.CenterDao;
import com.gpchanneling.gpc.data.dao.DoctorDao;
import com.gpchanneling.gpc.data.entity.Center;
import com.gpchanneling.gpc.dto.Doctor;
import com.gpchanneling.gpc.services.DoctorService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 10/4/2018.
 */

@Service
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    private DoctorDao doctorDao;
    @Autowired
    private CenterDao centerDao;

    @Override
    public List<Doctor> getDoctorsByCenterId(Map params) {

        int centerId = Integer.parseInt(params.get("centerId").toString());
        Center center = centerDao.getOne(centerId);
        ModelMapper mapper = new ModelMapper();
        List<Doctor> doctorList = new ArrayList<>();

        doctorDao.findByCenters(center).forEach(doctor -> {
            doctor.getUser().setRoles(null);
            doctorList.add(mapper.map(doctor, Doctor.class));
        });
        return doctorList;
    }
}
