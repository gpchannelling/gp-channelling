
package com.gpchanneling.gpc.services.impl;

import com.gpchanneling.gpc.data.dao.*;
import com.gpchanneling.gpc.data.entity.*;
import com.gpchanneling.gpc.dto.Appointment;
import com.gpchanneling.gpc.enums.Status;
import com.gpchanneling.gpc.services.AppointmentService;
import com.gpchanneling.gpc.services.PatientService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 11/21/2018.
 */
@Service
public class AppointmentServiceImpl implements AppointmentService {
    @Autowired
    private AppointmentDao appointmentDao;
    @Autowired
    private SessionDao sessionDao;
    @Autowired
    private DoctorDao doctorDao;
    @Autowired
    private CenterDao centerDao;
    @Autowired
    private PatientService patientService;
    @Autowired
    private PaymentDao paymentDao;
    @Autowired
    private AppointmentChargeDao appointmentChargeDao;

    @Override
    public List<Appointment> getAppointments(Map params) {

        int sessionId = Integer.parseInt(params.get("sessionId").toString());
        int doctorId = Integer.parseInt(params.get("doctorId").toString());


        ModelMapper mapper = new ModelMapper();
        List<Appointment> appointmentList = new ArrayList<>();

        com.gpchanneling.gpc.data.entity.Doctor doctor = doctorDao.getOne(doctorId);
        doctor.setUser(null);//set user field null
        Session session = sessionDao.getOne(sessionId);

        appointmentDao.findBySession(session).forEach(appointment -> {
            Appointment appointmentModle = new Appointment();
            mapper.map(session, appointmentModle);
            mapper.map(appointment, appointmentModle);

            //set payment details to appointment model
            mapper.map(appointmentChargeDao.findByAppointment(appointment), appointmentModle);
            appointmentList.add(mapper.map(appointmentModle, Appointment.class));

        });

        return appointmentList;
    }

    @Override
    public Appointment getAppointmentByRefNo(Map params) {

        int sessionId = Integer.parseInt(params.get("sessionId").toString());
        int doctorId = Integer.parseInt(params.get("doctorId").toString());
        int centerId = Integer.parseInt(params.get("centerId").toString());
        int refNo = Integer.parseInt(params.get("refNo").toString());

        ModelMapper mapper = new ModelMapper();
        Appointment appointmentModle = new Appointment();
        Doctor doctor = doctorDao.getOne(doctorId);
        doctor.setUser(null);
        Session session = mapper.map(doctor, Session.class);
        mapper.map(session, appointmentModle);
        mapper.map(appointmentDao.getOne(refNo), appointmentModle);
        com.gpchanneling.gpc.data.entity.Center center = centerDao.getOne(centerId);
        center.setDoctors(null);
        mapper.map(center, appointmentModle);

        return mapper.map(appointmentModle, Appointment.class);


    }

    @Override
    public Appointment saveAppointment(Map params, Appointment appointment) {
        int sessionId = Integer.parseInt(params.get("sessionId").toString());
        ModelMapper mapper = new ModelMapper();
        Session session = sessionDao.getOne(sessionId);
        Appointment appointmentResponse = new Appointment();
        com.gpchanneling.gpc.data.entity.Appointment appModel = new com.gpchanneling.gpc.data.entity.Appointment();

        //  check whether registered patient or non registered patient

            if (session.getNoOfPatient() > session.getNoOfReservedPatient()) {

                appModel.setSession(session);
                appModel.setStatus(appointment.getStatus());

                //save unregistered patient in patient table
                appModel.setPatient(mapper.map(patientService.savePatient(appointment.getPatient()), Patient.class));

                mapper.map(appointmentDao.save(appModel), appointmentResponse);
                appointmentResponse.getSession().getDoctor().getUser().setRoles(null);


                //save fees to appointmentFee table
                appointment.getAppointmentCharge().forEach(charge -> {
                            AppointmentCharge chargeModel = mapper.map(charge, AppointmentCharge.class);
                            appointmentChargeDao.save(chargeModel);
                        }
                );
            }

        return appointmentResponse;
    }

    @Override
    public Appointment updateAppointment(Map params, Appointment appointment) {
        int sessionId = Integer.parseInt(params.get("sessionId").toString());
        int refNo = Integer.parseInt(params.get("refNo").toString());
        int centerId = Integer.parseInt(params.get("centerId").toString());

        ModelMapper mapper = new ModelMapper();
        Appointment appointmentResponse = new Appointment();
        Session session = sessionDao.getOne(sessionId);
        Center center = centerDao.getOne(centerId);

        if (Status.PENDING.toString().equals(appointment.getStatus()) && session.getNoOfReservedPatient() < session.getNoOfPatient()) {
            com.gpchanneling.gpc.data.entity.Appointment appModel = appointmentDao.getOne(refNo);

            appModel.setStatus(Status.ACTIVE.toString());
            appModel.setAppointmentNo(session.getNoOfReservedPatient() + 1);//Add One to current reserved no of patients
            appModel.setAppointmentTime(session.getStartTime().plusMinutes(session.getTimeInterval() * session.getNoOfReservedPatient()));

            //save payment related to ref no in appointment payment table
            appointment.getPayments().forEach(payment -> {
                Payment paymentModel = mapper.map(payment, Payment.class);
                paymentModel.setAppointment(appModel);
                paymentDao.save(paymentModel);
            });

            //increment no of reserved patient by one when appointment proceed
            session.setNoOfReservedPatient(session.getNoOfReservedPatient() + 1);
            sessionDao.save(session);

            mapper.map(appointmentDao.save(appModel), appointmentResponse);
            appointmentResponse.getSession().getDoctor().getUser().setRoles(null);

        }
        return appointmentResponse;
    }


}
