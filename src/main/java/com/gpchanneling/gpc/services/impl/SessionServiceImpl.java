package com.gpchanneling.gpc.services.impl;

import com.gpchanneling.gpc.data.dao.AppointmentFeeDao;
import com.gpchanneling.gpc.data.dao.CenterDao;
import com.gpchanneling.gpc.data.dao.DoctorDao;
import com.gpchanneling.gpc.data.dao.SessionDao;
import com.gpchanneling.gpc.data.entity.AppointmentFee;
import com.gpchanneling.gpc.data.entity.Center;
import com.gpchanneling.gpc.data.entity.Doctor;
import com.gpchanneling.gpc.dto.Session;
import com.gpchanneling.gpc.services.SessionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 9/26/2018.
 */
@Service
public class SessionServiceImpl implements SessionService {

    @Autowired
    SessionDao sessionDao;
    @Autowired
    DoctorDao doctorDao;
    @Autowired
    CenterDao centerDao;
    @Autowired
    ResponseEntityExceptionHandler exceptionHandler;
    @Autowired
    AppointmentFeeDao appointmentFeeDao;

    @Override
    public List<Session> getSessionsByDoctorIdAndCenterId(Map params) {

        int centerId = Integer.parseInt(params.get("centerId").toString());
        int doctorId = Integer.parseInt(params.get("doctorId").toString());

        List<Session> sessionList = new ArrayList<>();
        Center center = centerDao.getOne(centerId);
        Doctor doctor = doctorDao.getOne(doctorId);
        ModelMapper mapper = new ModelMapper();

        //Get sessions related to particular center and doctor
        sessionDao.findByDoctorAndCenter(doctor, center).forEach(session -> {

            //Get appointment fees of given session
            List<com.gpchanneling.gpc.dto.AppointmentFee> appointmentFees = new ArrayList<>();
            appointmentFeeDao.findBySession(session).forEach(fees -> {
                appointmentFees.add(mapper.map(fees, com.gpchanneling.gpc.dto.AppointmentFee.class));
            });

            //Get session by session id
            Session sessionResponse = mapper.map(session, Session.class);
            sessionResponse.setAppointmentFees(appointmentFees);//set appointment fees to session
            sessionResponse.getDoctor().getUser().setRoles(null);
            sessionList.add(sessionResponse);


        });
        return sessionList;
    }

    @Override
    public Session getSessionBySessionId(Map params) {
        ModelMapper mapper = new ModelMapper();
        int sessionId = Integer.parseInt(params.get("sessionId").toString());
        com.gpchanneling.gpc.data.entity.Session session = sessionDao.getOne(sessionId);


        //Get appointment fees of given session
        List<com.gpchanneling.gpc.dto.AppointmentFee> appointmentFees = new ArrayList<>();
        appointmentFeeDao.findBySession(session).forEach(fees -> {
            appointmentFees.add(mapper.map(fees, com.gpchanneling.gpc.dto.AppointmentFee.class));
        });

        //Get session by session id
        Session sessionResponse = mapper.map(session, Session.class);
        sessionResponse.setAppointmentFees(appointmentFees);//set appointment fees to session

        sessionResponse.getDoctor().getUser().setRoles(null);
        return sessionResponse;
    }

    @Override
    public Session saveSession(Map params, Session session) {
        int centerId = Integer.parseInt(params.get("centerId").toString());
        int doctorId = Integer.parseInt(params.get("doctorId").toString());

        ModelMapper mapper = new ModelMapper();
        com.gpchanneling.gpc.data.entity.Session sessionModel = new com.gpchanneling.gpc.data.entity.Session();
        List<com.gpchanneling.gpc.dto.AppointmentFee> appointmentFees = new ArrayList<>();

        sessionModel.setDoctor(doctorDao.getOne(doctorId));
        sessionModel.setCenter(centerDao.getOne(centerId));
        sessionModel.setAppointmentDate(session.getAppointmentDate());
        sessionModel.setCurrentPatient(session.getCurrentPatient());
        sessionModel.setNoOfPatient(session.getNoOfPatient());
        sessionModel.setStatus(session.getStatus());
        sessionModel.setStartTime(session.getStartTime());
        sessionModel.setNoOfReservedPatient(session.getNoOfReservedPatient());
        sessionModel.setEndTime(session.getEndTime());

        session.getAppointmentFees().forEach(fees->{
            AppointmentFee feeResponse=appointmentFeeDao.save(mapper.map(fees, com.gpchanneling.gpc.data.entity.AppointmentFee.class));
            appointmentFees.add(mapper.map(feeResponse, com.gpchanneling.gpc.dto.AppointmentFee.class));
        });

        //generated response for inserting session
        Session sessionResponse = new Session();
        sessionResponse.setAppointmentFees(session.getAppointmentFees());
        mapper.map(sessionDao.save(sessionModel), sessionResponse);
        sessionResponse.getDoctor().getUser().setRoles(null);
        sessionResponse.setAppointmentFees(appointmentFees);
        return sessionResponse;

    }

    @Override
    public Session updateSession(Map params, Session session) {
        int sessionId = Integer.parseInt(params.get("sessionId").toString());

        ModelMapper mapper = new ModelMapper();
        com.gpchanneling.gpc.data.entity.Session sessionModel = sessionDao.getOne(sessionId);

        if (session.getAppointmentDate().isAfter(LocalDate.now()) && session.getStartTime().isBefore(session.getEndTime()) &&
                session.getNoOfPatient() >= session.getNoOfReservedPatient()) {

            sessionModel.setSessionId(sessionId);
            sessionModel.setAppointmentDate(session.getAppointmentDate());
            sessionModel.setStartTime(session.getStartTime());
            sessionModel.setEndTime(session.getEndTime());
            sessionModel.setStatus(session.getStatus());
            sessionModel.setNoOfPatient(session.getNoOfPatient());
            Session sessionResponse = new Session();
            mapper.map(sessionDao.save(sessionModel), sessionResponse);
            sessionResponse.setDoctor(null);
            return sessionResponse;

        } else {
            return null;          //TODO
        }


    }
}
