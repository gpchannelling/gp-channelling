package com.gpchanneling.gpc.services.impl;

import com.gpchanneling.gpc.data.dao.CenterDao;
import com.gpchanneling.gpc.data.dao.LocationDao;
import com.gpchanneling.gpc.dto.Center;
import com.gpchanneling.gpc.services.CenterService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 9/27/2018.
 */
@Service
public class CenterServiceImpl implements CenterService {

    @Autowired
    private CenterDao centerDao;
    @Autowired
    private LocationDao locationDao;


    @Override
    public List<Center> getCenters(Map params) {


        ModelMapper mapper = new ModelMapper();


        if (params.isEmpty()) {
            //get all registered medical centers
            List<Center> centerList = new ArrayList<>();
            centerDao.findAll().forEach(center->{
                        center.setDoctors(null);
                        centerList.add(mapper.map(center, Center.class));
            });
            return centerList;

        } else {

            //get registered medical centers by given cordinates
            double lat = Double.parseDouble(params.get("latitude").toString());
            double lon = Double.parseDouble(params.get("longitude").toString());
            return locationDao.findLocationByCoordinates(lat, lon);

        }



    }

}