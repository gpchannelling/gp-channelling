package com.gpchanneling.gpc.services.impl;

import com.gpchanneling.gpc.data.dao.PatientDao;
import com.gpchanneling.gpc.dto.Patient;
import com.gpchanneling.gpc.services.PatientService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ganusha on 10/12/2018.
 */
@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientDao patientDao;


    @Override
    public List<Patient> getPatients() {
        ModelMapper mapper = new ModelMapper();
        List<Patient> patientList =new ArrayList<>();
        for (com.gpchanneling.gpc.data.entity.Patient patient : patientDao.findAll()) {
            patientList.add(mapper.map(patient,Patient.class ));
        }
        return patientList;

    }

    @Override
    public Patient getPatientById(int patientid) {
        ModelMapper mapper = new ModelMapper();
        return  mapper.map(patientDao.findById(patientid),Patient.class );

    }

    @Override
    public Patient savePatient(Patient patient) {
        ModelMapper mapper=new ModelMapper();
        return mapper.map(patientDao.save(mapper.map(patient, com.gpchanneling.gpc.data.entity.Patient.class)),Patient.class);

    }


//
//    @Override
//    public JsonResponse updatePatient(Patient patient, int patientId) {
//        Optional<Patient> optionalPatient = patientDao.findById(patientId);
//        if (optionalPatient.isPresent()) {
//
//            patient.setPatientId(patientId);
//            response.setCode(HttpStatus.OK);
//            response.setMessage(HttpStatus.OK.toString());
//            response.setResponse(patientDao.save(patient));
//            return response;
//        }
//
//        response.setCode(HttpStatus.NOT_MODIFIED);
//        response.setMessage(HttpStatus.NOT_MODIFIED.toString());
//        return response;
//    }

}
