package com.gpchanneling.gpc.services;


import com.gpchanneling.gpc.dto.Doctor;

import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 10/4/2018.
 */
public interface DoctorService {

    List<Doctor> getDoctorsByCenterId(Map params);
}
