package com.gpchanneling.gpc.services;



import com.gpchanneling.gpc.dto.Appointment;

import java.util.List;
import java.util.Map;

/**
 * Created by ganusha on 11/21/2018.
 */
public interface AppointmentService {
    List<Appointment> getAppointments(Map params);
    Appointment getAppointmentByRefNo(Map params);
    Appointment saveAppointment(Map params,Appointment appointment);
    Appointment updateAppointment(Map params, Appointment appointment);
}
